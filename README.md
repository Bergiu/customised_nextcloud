# Customised Nextcloud
This is my customised nextcloud docker image with some plugins.

## Plugins
- [ocdownloader](https://github.com/e-alfred/ocdownloader)

## Docker
### Official build:
- [hub.docker.com/r/bergiu/customised\_nextcloud/](https://hub.docker.com/r/bergiu/customised_nextcloud/)

### Build:
- `docker build -t bergiu/customised_nextcloud:v1.0.0 .`
- `docker build -t bergiu/customised_nextcloud:latest .`

### Run:
- `docker create --name nextcloud --publish 8000:80 -e "VIRTUAL_HOST=cloud.deinedomain.tk" --restart always --volume /docker/nextcloud/:/var/www/html/ bergiu/customised_nextcloud`

### Others:
- `docker exec -it nextcloud bash`

### Upload:
1. `docker login`
2. `docker tag bergiu/customised_nextcloud bergiu/customised_nextcloud`
3. `docker push bergiu/customised_nextcloud`

## youtube-dl path
- `/usr/local/bin/youtube-dl`
