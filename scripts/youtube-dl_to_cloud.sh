#!/bin/bash
# $1 := download link
# $2 := nextcloud user
cd /var/www/html/data/$2/files/Media/ocDownloads
sudo -u www-data QT_QPA_PLATFORM=offscreen youtube-dl $1
cd /var/www/html/
sudo -u www-data ./occ files:scan $2
