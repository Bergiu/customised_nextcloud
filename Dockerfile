FROM nextcloud:apache
MAINTAINER Marco Schlicht (marcoschlicht@onlinehome.de)

RUN apt-get update -y

# ARIA2
RUN apt-get -qq install aria2 curl sudo wget supervisor cron
RUN mkdir /var/log/aria2c /var/local/aria2c
RUN touch /var/log/aria2c/aria2c.log
RUN touch /var/local/aria2c/aria2c.sess
RUN chown www-data.www-data -R /var/log/aria2c /var/local/aria2c
RUN chmod 770 -R /var/log/aria2c /var/local/aria2c
RUN sudo -u www-data aria2c --enable-rpc --rpc-allow-origin-all -c -D --log=/var/log/aria2c/aria2c.log --check-certificate=false --save-session=/var/local/aria2c/aria2c.sess --save-session-interval=2 --continue=true --input-file=/var/local/aria2c/aria2c.sess --rpc-save-upload-metadata=true --force-save=true --log-level=warn
RUN mkdir /var/log/supervisord /var/run/supervisord

# YOUTUBE-DL
RUN apt-get -qq install python3 python3-pip
RUN pip3 install youtube-dl

# supervisord to execute cron
COPY supervisord.conf /etc/supervisor/supervisord.conf

# my script that is executed every minute from cron
ADD my-init.sh /

# my cron job
ADD aria2cron /etc/cron.d/aria2cron
RUN chmod 0644 /etc/cron.d/aria2cron
RUN crontab /etc/cron.d/aria2cron

# copy my scripts
ADD scripts/ /scripts

# PHANTOMJS for youtube-dl
RUN apt-get -qq install nodejs phantomjs

# execute supervisord
CMD ["/usr/bin/supervisord"]
